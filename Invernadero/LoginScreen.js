import React, { useState } from 'react'; 
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, Alert } from 'react-native'; 
import { Ionicons } from '@expo/vector-icons'; 
import firebase from './Components/Firebase'; 
import "firebase/auth"; 
import { MaterialCommunityIcons } from '@expo/vector-icons'; 
import { ViewPropTypes } from 'react-native'; 
 
const login = ({ navigation }) => { 
  const [email, guardarEmail] = useState(''); 
  const [password, guardarPassword] = useState(''); 
  const [error, guardarError] = useState(''); 
  const [passwordVisibility, setPasswordVisibility] = useState(false); 
 
  function handlePasswordVisibility() { 
    setPasswordVisibility(!passwordVisibility); 
  } 
 
  const onBottomPress = () => { 
    firebase.auth().signInWithEmailAndPassword(email, password) 
      .then(onLoginSuccess) 
      .catch(err => { 
        guardarError(err.message) 
      }); 
 
  } 
 
  const onLoginSuccess = () => { 
    var user = firebase.auth().currentUser; 
    if (user.emailVerified === true) { 
      guardarError(''); 
    } else { 
      Alert.alert( 
        "Warning!", 
        "Verify your account", 
        [ 
          { text: "OK" }, 
        ] 
      ); 
    } 
 
 
  } 
 
  return ( 
    <View style={{ backgroundColor: '#fff', flex: 1 }}> 
      <View style={styles.container}> 
        <Image style={styles.imagen} source={require('./assets/images/leaves.png')} /> 
      </View> 
      <View style={{ backgroundColor: '#fff', flex: 1 }}> 
        <View style={styles.inputView} > 
          <Ionicons name="person" size={24} color="black" style={{ paddingLeft: 15 }} /> 
          <TextInput 
            value={email} 
            style={styles.inputText} 
            placeholder="Email" 
            placeholderTextColor="#003f5c" 
            onChangeText={text => guardarEmail(text)} /> 
        </View> 
        <View style={styles.inputView} > 
          <Ionicons name="lock-closed" size={24} color="black" style={{ paddingLeft: 15 }} /> 
          <TextInput 
            value={password} 
            secureTextEntry={passwordVisibility ? false : true} 
            style={styles.inputText} 
            placeholder="Password" 
            placeholderTextColor="#003f5c" 
            onChangeText={text => guardarPassword(text)} /> 
          <TouchableOpacity onPress={handlePasswordVisibility}> 
            <MaterialCommunityIcons 
              name={passwordVisibility ? 'eye-off' : 'eye'} 
              size={20} 
              color="#4E4E4E" 
              style={styles.rightIconStyles} 
            /> 
          </TouchableOpacity> 
        </View> 
        <View style={styles.viewForgot}> 
          <TouchableOpacity style={styles.forgotBtn} onPress={() => navigation.navigate('ForgotPass')}> 
            <Text style={styles.forgot}>Forgot Password?</Text> 
          </TouchableOpacity> 
        </View> 
 
        <View style={styles.viewError}> 
          <Text style={styles.textError}>{error}</Text> 
        </View> 
 
        <View style={styles.btnView}> 
          <TouchableOpacity style={styles.loginBtn} onPress={() => onBottomPress()} > 
            <Text style={styles.loginText}>LOGIN</Text> 
          </TouchableOpacity> 
        </View> 
 
      </View> 
    </View> 
  ); 
} 
 
const styles = StyleSheet.create({ 
  container: { 
    flex: 1, 
    backgroundColor: '#fff', 
    justifyContent: 'center', 
    alignItems: 'center', 
    paddingTop: 20, 
  }, 
  inputView: { 
    flexDirection: 'row', 
    alignItems: 'center', 
    backgroundColor: '#edeff1', 
    borderColor: 'black', 
    borderWidth: 1, 
    borderRadius: 25, 
    marginHorizontal: 10, 
    marginVertical: 25, 
  }, 
  inputText: { 
    flex: 1, 
    paddingTop: 10, 
    paddingRight: 10, 
    paddingBottom: 10, 
    paddingLeft: 20, 
    fontWeight: 'bold', 
    fontSize: 18, 
    color: 'black' 
  }, 
  loginBtn: { 
    width: "100%", 
    borderRadius: 25, 
    height: 50, 
    alignItems: "center", 
    justifyContent: "center", 
    backgroundColor: "#c52a2d", 
  }, 
  loginText: { 
    color: "white", 
    fontSize: 18, 
  }, 
  imagen: { 
    width: 150, 
    height: 150, 
    marginBottom: 80, 
  }, 
  forgot: { 
    color: '#c52a2d', 
    fontWeight: 'bold', 
    textAlign: 'right' 
  }, 
  forgotBtn: { 
    width: "40%", 
  }, 
  viewForgot: { 
    marginRight: 10, 
    alignItems: "flex-end" 
  }, 
  textError: { 
    justifyContent: "center", 
    alignItems: "center", 
    textAlign: 'center' 
  }, 
  viewError: { 
    paddingTop: 25, 
    paddingHorizontal: 20, 
  }, 
  rightIconStyles: { 
    alignSelf: 'center', 
    marginRight: 20 
  }, 
  btnView: { 
    alignItems: "center", 
    justifyContent: "center", 
    marginHorizontal: 10, 
    marginVertical: 10, 
  }, 
}); 
 
export default login;