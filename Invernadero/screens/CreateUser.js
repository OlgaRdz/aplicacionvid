import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert, Button } from 'react-native'
import React from 'react'
import { useState } from 'react'
import { Picker } from '@react-native-community/picker'
import { Ionicons } from '@expo/vector-icons';
import firebase from '../Components/Firebase';
import firebaseSecond from "firebase/app";
import "firebase/auth";
import "firebase/database";
import { Formik } from 'formik';
import * as yup from 'yup';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function CreateUser({ navigation }) {

    const [passwordVisibility, setPasswordVisibility] = useState(false);
    const [confirmPasswordVisibility, setConfirmPasswordVisibility] = useState(false);

    function handlePasswordVisibility() {
        setPasswordVisibility(!passwordVisibility);
    }
    function handleConfirmPasswordVisibility() {
        setConfirmPasswordVisibility(!confirmPasswordVisibility);
    }

    var db = firebase.database().ref('Admins/UID/');

    const [selectedValue, setSelectedValue] = useState("user");
    const [error, guardarError] = useState('');

    var config = {
        apiKey: "AIzaSyBL9qQgrHzYUan_ErVEYCEkQwLByF0yguQ",
        authDomain: "vineyard-6c024.firebaseapp.com",
        databaseURL: "https://vineyard-6c024-default-rtdb.firebaseio.com",
        projectId: "vineyard-6c024",
        storageBucket: "vineyard-6c024.appspot.com",
        messagingSenderId: "85440077086",
        appId: "1:85440077086:web:2480a7d60a7b6649ccfd0f"
    };

    const Alerta = (nameAlert, emailAlert) => {
        Alert.alert(
            "User Created",
            "The user " + nameAlert + " has been created successfully. Verify the account from your email: " + emailAlert,
            [
                { text: "OK" },
            ]
        );
    }

    const createUsers = (values) => {
        var userName = values.name;
        var email = values.email;
        var password = values.password;

        try {
            var secondaryApp = firebaseSecond.initializeApp(config, "Secondary");
        } catch (error) {
            var secondaryApp = firebaseSecond.app("Secondary");
        }

        secondaryApp.auth()
            .createUserWithEmailAndPassword(email, password)
            .then(function (firebaseUser) {
                var user2 = secondaryApp.auth().currentUser;
                var correo2 = user2.email;
                var uid2 = user2.uid;
                console.log("User " + correo2 + " created successfully!");
                console.log(user2.emailVerified);

                if (selectedValue === "admin") {
                    db.update({
                        [uid2]: true,
                    });
                }
                else {
                    db.update({
                        [uid2]: false,
                    });
                }

                user2.updateProfile({
                    displayName: userName,
                }).then(function () {
                    user2.sendEmailVerification().then(function () {
                        Alerta(user2.displayName, user2.email);
                    }).catch(function (error) {
                        console.error(error);
                    });

                    secondaryApp.auth().signOut();

                }).catch(function (error) {
                    guardarError(error.message);
                });
                navigation.navigate('TabStack')
            })
            .catch(error => {
                guardarError(error.message);
            });
    }

    const createUserValidationSchema = yup.object().shape({
        name: yup
            .string()
            .required('Username is required'),
        email: yup
            .string()
            .email("Please enter valid email")
            .required('Email Address is Required'),
        password: yup
            .string()
            .matches(/\w*[a-z]\w*/, "Password must have a lowercase letter")
            .matches(/\w*[A-Z]\w*/, "Password must have a capital letter")
            .matches(/\d/, "Password must have a number")
            .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
            .min(8, ({ min }) => `Password must be at least ${min} characters`)
            .required('Password is required'),
        confirmPassword: yup
            .string()
            .oneOf([yup.ref('password')], 'Passwords do not match')
            .required('Confirm password is required'),
    })

    return (
        <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <View style={styles.content}>
                <Text style={styles.title}>Create account</Text>
            </View>
            <Formik
                validationSchema={createUserValidationSchema}
                initialValues={{
                    name: '',
                    email: '',
                    password: '',
                    confirmPassword: ''
                }}
                onSubmit={values => createUsers(values)}
            >
                {({ handleChange, handleBlur, handleSubmit, values, errors, touched, isValid }) => (
                    <View style={{ backgroundColor: '#fff', flex: 1 }}>
                        <View style={styles.inputContainer}>
                            <Ionicons name="person" size={24} color="black" style={{ paddingLeft: 15 }} />
                            <TextInput
                                name='name'
                                multiline={false}
                                style={styles.input}
                                value={values.name}
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                                placeholder='Username'
                            />
                        </View>
                        {errors.name && touched.name &&
                            <Text style={styles.textError}>{errors.name}</Text>
                        }
                        <View style={styles.inputContainer}>
                            <Ionicons name="mail" size={24} color="black" style={{ paddingLeft: 15 }} />
                            <TextInput
                                name='email'
                                style={styles.input}
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                placeholder='Email'
                                keyboardType="email-address"
                            />
                        </View>
                        {errors.email && touched.email &&
                            <Text style={styles.textError}>{errors.email}</Text>
                        }
                        <View style={styles.inputContainer}>
                            <Ionicons name="lock-closed" size={24} color="black" style={{ paddingLeft: 15 }} />
                            <TextInput
                                name='password'
                                style={styles.input}
                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                secureTextEntry={passwordVisibility ? false : true}
                                placeholder='Password'
                            />
                            <TouchableOpacity onPress={handlePasswordVisibility}>
                                <MaterialCommunityIcons
                                    name={passwordVisibility ? 'eye-off' : 'eye'}
                                    size={20}
                                    color="#4E4E4E"
                                    style={styles.rightIconStyles}
                                />
                            </TouchableOpacity>
                        </View>
                        {errors.password && touched.password &&
                            <Text style={styles.textError}>{errors.password}</Text>
                        }
                        <View style={styles.inputContainer}>
                            <Ionicons name="lock-closed" size={24} color="black" style={{ paddingLeft: 15 }} />
                            <TextInput
                                name='confirmPassword'
                                style={styles.input}
                                value={values.confirmPassword}
                                onChangeText={handleChange('confirmPassword')}
                                onBlur={handleBlur('confirmPassword')}
                                secureTextEntry={confirmPasswordVisibility ? false : true}
                                placeholder='Confirm Password'
                            />
                            <TouchableOpacity onPress={handleConfirmPasswordVisibility}>
                                <MaterialCommunityIcons
                                    name={confirmPasswordVisibility ? 'eye-off' : 'eye'}
                                    size={20}
                                    color="#4E4E4E"
                                    style={styles.rightIconStyles}
                                />
                            </TouchableOpacity>
                        </View>
                        {errors.confirmPassword && touched.confirmPassword &&
                            <Text style={styles.textError}>{errors.confirmPassword}</Text>
                        }
                        <View style={styles.pickerContainer} >
                            <Text style={styles.user}>Role</Text>
                            <Picker
                                selectedValue={selectedValue}
                                style={styles.picker}
                                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="User" value="user" />
                                <Picker.Item label="Admin" value="admin" />
                            </Picker>
                        </View>

                        <View style={styles.viewError}>
                            <Text style={styles.textError}>{error}</Text>
                        </View>

                        <View style={styles.btnView}>
                            <TouchableOpacity style={!isValid ? styles.buttonDisable : styles.button} onPress={handleSubmit} disabled={!isValid}>
                                <Text style={{ color: '#fff', fontSize: 18, }}>Register</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </Formik>
        </View>
    )
}

const styles = StyleSheet.create({
    content: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
        backgroundColor: '#fff'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#edeff1',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 25,
        marginHorizontal: 10,
        marginVertical: 25,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 35,
        color: '#303d4c',
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black'
    },
    user: {
        fontSize: 18,
        color: 'black',
        marginHorizontal: 16,
        marginTop: 10
    },
    picker: {
        marginHorizontal: 10
    },
    pickerContainer: {
        borderWidth: 1,
        borderRadius: 25,
        marginHorizontal: 10,
        backgroundColor: '#edeff1',
        marginTop: 35
    },
    button: {
        width: "100%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#c52a2d'
    },
    buttonDisable: {
        width: "100%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#D6D7D8'
    },
    textError: {
        justifyContent: "center",
        alignItems: "center",
        textAlign: 'center',
        color: 'red'
    },
    viewError: {
        paddingTop: 25,
        paddingHorizontal: 20,
    },
    rightIconStyles: {
        alignSelf: 'center',
        marginRight: 20
    },
    btnView: {
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 10,
        marginVertical: 10,
    },
});