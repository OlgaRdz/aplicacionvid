import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text, TouchableOpacity, SafeAreaView, FlatList } from "react-native";
import { Checkbox, Avatar } from 'react-native-paper';
import firebase from '../Components/Firebase';
import "firebase/database"

const Furrows = ({ navigation, route }) => {

  console.log(route.params);

  const [checked, setChecked] = React.useState(true);
  const Valors = route.params
  const [furrows, guardarFurrows] = useState([]);

  function Furrows() {
    var vafurrow = firebase.database().ref('Furrows');
    vafurrow.orderByChild('id_plot').equalTo(route.params.id).on('value', (snapshot) => {
      var furrowsplot = snapshot.val();

      guardarFurrows(furrowsplot);
      console.log(furrowsplot);
    });
  }

  useEffect(() => {
    const obtenerFurrows = () => {
      try {
        Furrows();
      } catch (error) {
        console.log(error);
      }
    }
    obtenerFurrows();
  }, []);


  const newFurrows = Object.keys(furrows).map((key) => {
    furrows[key].id = key;
    return furrows[key];
  });

  const Item = ({ item, onPress, style, idfurrow = item.id, namefurrow = item.name }) => (

    <View style={{ paddingHorizontal: 10, backgroundColor: "#fff", flex: 1 }}>

      <TouchableOpacity style={styles.container1} onPress={() => navigation.navigate("Furrows-Bugs", { Valors, idfurrow, namefurrow })} >
        <View style={{ marginRight: 260, top: 20 }}>
          <Avatar.Icon size={40} icon="bug" />
        </View>
        <View style={{ marginRight: 70, top: -30 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 24 }}>{item.name} </Text>


        </View>
        <View style={{ marginLeft: 30, alignItems: "center", top: -11 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 18 }}>Mildew</Text>
        </View>
        <View style={{ marginRight: 100, alignItems: "center", top: -40 }}>
          <Checkbox
            status={checked ? 'checked' : 'unchecked'}
            onPress={() => {
              setChecked(checked);
            }}
          />
        </View>

      </TouchableOpacity>


    </View>

  );

  const [selectedId, setSelectedId] = useState(null);
  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';
    if (item.healthyFurrow === false) {
      return <Item item={item} onPress={() => setSelectedId(item.id)} style={{ backgroundColor }} />;
    }
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        data={newFurrows}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        extraData={selectedId}
      />
    </SafeAreaView>
  );
}



const styles = StyleSheet.create({
  container1: {
    width: '100%',
    height: 140,
    alignItems: "center",
    justifyContent: "center",
    marginVertical: 5,
    borderRadius: 60,
    borderColor: '#b3bcc2',
    borderWidth: 3,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
export default Furrows;