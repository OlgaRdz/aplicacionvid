import React from "react";
import { ScrollView } from "react-native";
import { View, StyleSheet, Image, Text, TouchableOpacity } from "react-native";

const Irrigation = (props) => {
  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>

      <View style={styles.container}>

        <View style={styles.listado}>

          <View style={styles.listadoItem}>
            <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate("WeatherCondition")}>
              <Image source={require('../assets/images/Iconos/thunderstorm.png')} style={styles.imagen} />
              <View style={{ paddingVertical: 60, marginRight: 10, alignItems: "flex-end" }}>
                <Text style={styles.textTitle}>Weather{"\n"}Condition</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.listadoItem}>
            <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate("CropIrrigation")}>
              <Image source={require('../assets/images/sprinklers.png')} style={styles.imagen} />
              <View style={{ paddingVertical: 60, marginRight: 10, alignItems: "flex-end" }}>
                <Text style={styles.textTitle}>Crop{"\n"}Irrigation</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.listadoItem}>
            <TouchableOpacity style={styles.button} onPress={() => props.navigation.navigate("CropHumidity")}>
              <Image source={require('../assets/images/humidity.png')} style={styles.imagen} />
              <View style={{ paddingVertical: 60, marginRight: 10, alignItems: "flex-end" }}>
                <Text style={styles.textTitle}>Crop{"\n"}Humidity</Text>
              </View>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#fff',
  },
  button: {
    backgroundColor: "#fff",
    borderRadius: 15,
    width: '100%',
    height: 230,
    marginVertical: 5,
    padding: 10,
    borderWidth: 3,
    borderColor: '#d1d6dd'
  },
  textTitle: {
    fontSize: 30,
    color: '#5a636e',
    fontWeight: 'bold',
    paddingLeft: 180,
  },
  listado: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  listadoItem: {
    flex: 1,
    backgroundColor: '#fff'
  },
  imagen: { 
    width: 140, 
    height: 140, 
    position: 'absolute', 
    left: 20, 
    top: 45 
  }
});

export default Irrigation;