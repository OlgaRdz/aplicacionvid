import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import firebase from '../Components/Firebase';
import "firebase/auth";

const ForgotPass = ({ navigation }) => {
  //
  const [email, guardarEmail] = useState('');
  const [error, guardarError] = useState('');

  const sendEmail = () => {
    var auth = firebase.auth();

    auth.sendPasswordResetEmail(email).then(function () {
      // Email sent.

      Alert.alert(
        "Email Sent!",
        "Please verify your email address to reset your password",
        [
          { text: "OK" },
        ]

      );
      navigation.navigate('Login');

    }).catch(function (error) {
      
      guardarError(error.message);
      
    });
  }
  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>Reset Your Password</Text>
      <View style={styles.inputView} >
        <Text style={styles.textTitle1}>Email</Text>
        <TextInput
          style={styles.inputText}
          placeholder="Enter Email Address"
          placeholderTextColor="#003f5c"
          onChangeText={guardarEmail} />
      </View>
      <TouchableOpacity style={styles.loginBtn} onPress={sendEmail}>
        <Text style={styles.loginText}>Send Reset Link
          </Text>
      </TouchableOpacity>

      <View style={styles.viewError}>
        <Text style={styles.textError}>{error}</Text>
      </View>
      
      <View style={{ height: 3, backgroundColor: '#edeff1', width: 330, marginTop: 30 }} />
      <TouchableOpacity>
        <Text style={styles.forgot} onPress={() =>
          navigation.navigate('Login')} >Back To Login</Text>
      </TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    top: -100
  },
  inputView: {
    width: "80%",
    backgroundColor: "#edeff1",
    borderRadius: 15,
    height: 50,
    justifyContent: "center",
    padding: 20,
    top: 80,
  },
  inputText: {
    height: 50,
    color: "black",
    top: -15,
  },
  loginBtn: {
    width: "80%",
    backgroundColor: "#c52a2d",
    borderRadius: 15,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 80,
    marginBottom: 10,
    top: 20,
  },
  loginText: {
    color: "white",
    fontSize: 20
  },
  text: {
    width: 150,
    height: 150,
    marginBottom: 80,
  },
  forgot: {
    color: '#c52a2d',
    textAlign: 'right',
    paddingTop: 10,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
    fontSize: 16
  },
  textTitle: {
    fontSize: 30,
    color: '#5a636e',
    fontWeight: 'bold',
  },
  textTitle1: {
    fontSize: 25,
    color: '#5a636e',
    textAlign: 'left',
    top: -30,
    fontWeight: 'bold'
  },
  textError: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: 'center'
  },
  viewError: {
    paddingTop: 25,
    paddingHorizontal: 20,
    
  }
});

export default ForgotPass;