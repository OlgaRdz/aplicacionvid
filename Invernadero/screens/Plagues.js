import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import firebase from '../Components/Firebase';
import "firebase/database"


function Plagues(props) {
  const [plot1, guardarPlot1] = useState([]);
  const [plot2, guardarPlot2] = useState([]);
  const [plot3, guardarPlot3] = useState([]);
  const [plot4, guardarPlot4] = useState([]);
  var plot = null;

  var furrowsplot1 = null
  var furrowsplot2 = null
  var furrowsplot3 = null
  var furrowsplot4 = null

  var infectedplot = null

  function consultarPlot1() {
    var vaplot = firebase.database().ref('Plots/p_001');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      var plotid = { id: "p_001" };
      plot = Object.assign({}, plot, plotid);
      guardarPlot1(plot);

    });
  }

  function consultarPlot2() {
    var vaplot = firebase.database().ref('Plots/p_002');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      var plotid = { id: "p_002" };
      plot = Object.assign({}, plot, plotid);
      guardarPlot2(plot);

    });

  }
  function consultarPlot3() {
    var vaplot = firebase.database().ref('Plots/p_003');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      var plotid = { id: "p_003" };
      plot = Object.assign({}, plot, plotid);
      guardarPlot3(plot);
    });

  }
  function consultarPlot4() {
    var vaplot = firebase.database().ref('Plots/p_004');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      var plotid = { id: "p_004" };
      plot = Object.assign({}, plot, plotid);
      guardarPlot4(plot);
    });
  }

  useEffect(() => {
    const consultarPlots = async () => {
      try {
        consultarPlot1();
        consultarPlot2();
        consultarPlot3();
        consultarPlot4();
      } catch (error) {
        console.error(error);
      }
    }
    consultarPlots();
  }, []);

  

  var BreakException1 = {};
  useEffect(() => {
    function infectedFurrow_p_001() {
      var vafurrow1 = firebase.database().ref('Furrows');
      vafurrow1.orderByChild('id_plot').equalTo('p_001').on('value', (snapshot) => {
        infectedplot = snapshot.val();

        try {
          snapshot.forEach((p01snapshot) => {

            const { healthyFurrow } = p01snapshot.val()


            if (healthyFurrow === false) {
              firebase.database().ref('Plots/p_001').update({
                healthyPlot: false,

              });
              throw BreakException1;
            } else if (healthyFurrow === true) {
              firebase.database().ref('Plots/p_001').update({
                healthyPlot: true,
              });
            }
          });
        } catch (e) {
          if (e !== BreakException1) throw e;
        }
      });
    }
    infectedFurrow_p_001();
  }, []);


  var BreakException2 = {};
  useEffect(() => {
    function infectedFurrow_p_002() {
      var vafurrow2 = firebase.database().ref('Furrows');
      vafurrow2.orderByChild('id_plot').equalTo('p_002').on('value', (snapshot) => {
        infectedplot = snapshot.val();

        try {
          snapshot.forEach((p02snapshot) => {

            const { healthyFurrow } = p02snapshot.val()

            if (healthyFurrow === false) {
              firebase.database().ref('Plots/p_002').update({
                healthyPlot: false,

              });
              throw BreakException2;
            } else if (healthyFurrow === true) {
              firebase.database().ref('Plots/p_002').update({
                healthyPlot: true,
              });
            }
          });
        } catch (e) {
          if (e !== BreakException2) throw e;
        }
      });
    }
    infectedFurrow_p_002();
  }, []);


  var BreakException3 = {};
  useEffect(() => {
    function infectedFurrow_p_003() {
      var vafurrow3 = firebase.database().ref('Furrows');
      vafurrow3.orderByChild('id_plot').equalTo('p_003').on('value', (snapshot) => {
        infectedplot = snapshot.val();

        try {
          snapshot.forEach((p03snapshot) => {

            const { healthyFurrow } = p03snapshot.val()

            if (healthyFurrow === false) {
              firebase.database().ref('Plots/p_003').update({
                healthyPlot: false,

              });
              throw BreakException3;
            } else if (healthyFurrow === true) {
              firebase.database().ref('Plots/p_003').update({
                healthyPlot: true,
              });
            }
          });
        } catch (e) {
          if (e !== BreakException3) throw e;
        }
      });
    }
    infectedFurrow_p_003();
  }, []);


  var BreakException4 = {};
  useEffect(() => {
    function infectedFurrow_p_004() {
      var vafurrow = firebase.database().ref('Furrows');
      vafurrow.orderByChild('id_plot').equalTo('p_004').on('value', (snapshot) => {
        infectedplot = snapshot.val();
        try {
          snapshot.forEach((p04snapshot) => {
            const { healthyFurrow } = p04snapshot.val()


            if (healthyFurrow === false) {
              firebase.database().ref('Plots/p_004').update({
                statePlot: false,

              });
              throw BreakException4;
            } else if (healthyFurrow === true) {
              firebase.database().ref('Plots/p_004').update({
                statePlot: true,
              });
            }
          });
        } catch (e) {
          if (e !== BreakException4) throw e;
        }
      });
    }
    infectedFurrow_p_004();
  }, []);

  return (
    <View style={styles.container}>


      <View style={styles.countContainer}>

        <Text style={styles.textTitle}>Plagues and diseases</Text>
      </View>

      <View style={styles.listado}>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot4.healthyPlot ? styles.buttonGreen : styles.button} onPress={() => props.navigation.navigate("Furrows", { name: plot4.Name, id: plot4.id })} disabled={plot4.healthyPlot} >
            <Text style={styles.textBody}>{plot4.Name}</Text>
            <Text style={styles.textBodyName}>{plot4.Type}</Text>
            <Text style={styles.textBody}>{plot4.healthyPlot ? "Perfect Condition" : "Infested Furrows"}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot2.healthyPlot ? styles.buttonGreen : styles.button} onPress={() => props.navigation.navigate("Furrows", { name: plot2.Name, id: plot2.id })} disabled={plot2.healthyPlot} >
            <Text style={styles.textBody}>{plot2.Name}</Text>
            <Text style={styles.textBodyName}>{plot2.Type}</Text>
            <Text style={styles.textBody}>{plot2.healthyPlot ? "Perfect Condition" : "Infested Furrows"}</Text>

          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot3.healthyPlot ? styles.buttonGreen : styles.button} onPress={() => props.navigation.navigate("Furrows", { name: plot3.Name, id: plot3.id })} disabled={plot3.healthyPlot} >
            <Text style={styles.textBody}>{plot3.Name}</Text>
            <Text style={styles.textBodyName}>{plot3.Type}</Text>
            <Text style={styles.textBody}>{plot3.healthyPlot ? "Perfect Condition" : "Infested Furrows"}</Text>

          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot1.healthyPlot ? styles.buttonGreen : styles.button} onPress={() => props.navigation.navigate("Furrows", { name: plot1.Name, id: plot1.id })} disabled={plot1.healthyPlot} >
            <Text style={styles.textBody}>{plot1.Name}</Text>
            <Text style={styles.textBodyName}>{plot1.Type}</Text>
            <Text style={styles.textBody}>{plot1.healthyPlot ? "Perfect Condition" : "Infested Furrows"}</Text>

          </TouchableOpacity>
        </View>

      </View>

    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 40,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#ef9495",
    borderRadius: 15,
    width: '100%',
    height: 150,
    marginVertical: 5,
    marginHorizontal: 2,
    padding: 10,
    borderWidth: 2,
    borderColor: 'black'
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  },
  buttonGreen: {
    alignItems: "center",
    backgroundColor: "#a8ea7f",
    borderRadius: 15,
    width: '100%',
    height: 150,
    marginVertical: 5,
    marginHorizontal: 2,
    padding: 10,
    borderWidth: 2,
    borderColor: 'black'
  },
  textTitle: {
    fontSize: 30,
    color: 'black',
    fontWeight: 'bold'
  },
  textBody: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    fontWeight: 'bold'
  },
  textBodyName: {
    textAlign: 'center',
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold'
  },
  listado: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  listadoItem: {
    flexBasis: '48%'
  }
});

export default Plagues;