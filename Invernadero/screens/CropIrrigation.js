import React, { useState, useEffect } from "react";
import { View, Switch, StyleSheet, Image, Text, TouchableOpacity } from "react-native";
import firebase from '../Components/Firebase';
import "firebase/database"
import "firebase/auth";

const CropIrrigation = (props) => {

  //VARIABLES BOMBA
  const [estadoBomba, guardaEstadoBomba] = useState('');
  var estado = null;

  //START CONTROL BOMBA
  function recibirEstadoBomba() {
    var statusBomba = firebase.database().ref('Sensors/s_002/stateSensor');
    statusBomba.on('value', (snapshot) => {
      estado = snapshot.val();
      guardaEstadoBomba(estado);
    });
  }
  useEffect(() => {
    const consultarEstadoBomba = async () => {
      try {
        recibirEstadoBomba();

      } catch (error) {
        console.error(error);

      }

    }
    consultarEstadoBomba();

  }, []);

  const cambiosActualizados = () => {
    guardaEstadoBomba(!estadoBomba);
    firebase.database().ref('Sensors/s_002').update({
      stateSensor: !estadoBomba,

    });
  }

  //END CONTROL BOMBA

  return (
    <View style={{ paddingHorizontal: 20, backgroundColor: "#fff", flex: 1 }}>

      <View style={styles.container1}>
        <Image source={require('../assets/images/Iconos/car.png')} style={styles.imagen} />
        <View style={styles.containerviewsection}>
          <Text style={styles.texto}>Pump Status</Text>
        </View>
        <View style={styles.containerviewsection}>

          <Switch
            style={{ marginVertical: 10, }}
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={estadoBomba ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#fff"
            onValueChange={cambiosActualizados}
            value={estadoBomba}
          />
        </View>
      </View>

      <View style={styles.container2}>
        <Image source={require('../assets/images/manguera.png')} style={styles.imagen} />
        <View style={styles.containerviewsection}>
          <Text style={styles.texto}>Flow</Text>
        </View>
      </View>

      <View style={styles.container3}>
        <Image source={require('../assets/images/clock.png')} style={styles.imagen} />
        <View style={styles.containerviewsection}>
          <Text style={styles.texto}>Watering Hours{"\n"}& Total Hours</Text>
        </View>
      </View>

      <View style={styles.container4}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("CropHumidity")}
          style={styles.Button1}>
          <Text style={styles.textbutton}> PLOTS  </Text>
        </TouchableOpacity>
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container1: {
    height: 100,
    width: '100%',
    alignItems: "center",
    justifyContent: "center",
    padding: 40,
    marginVertical: 10,
    borderRadius: 60,
    borderColor: '#b3bcc2',
    borderWidth: 3,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flex: 10,
  },
  container2: {
    flex: 10,
    alignItems: "center",
    justifyContent: "center",
    padding: 40,
    marginVertical: 10,
    borderRadius: 60,
    borderColor: '#b3bcc2',
    borderWidth: 3,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  container3: {
    flex: 10,
    alignItems: "center",
    justifyContent: "center",
    padding: 40,
    marginVertical: 10,
    borderRadius: 60,
    borderColor: '#b3bcc2',
    borderWidth: 3,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  container4: {
    flex: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  Button1: {
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#bd0045',
  },
  textbutton: {
    textAlign: 'center',
    fontSize: 20,
    marginTop: 0,
    width: 200,
    color: "#fff",
  },
  Button2: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    left: 135,
    top: 150,
    borderColor: '#bd0045',
    borderWidth: 3,
    borderRadius: 10
  },
  containerviewsection: { 
    marginLeft: 100, 
    alignItems: "center" 
  },
  imagen:{ 
    width: 60, 
    height: 60, 
    position: 'absolute', 
    left: 20 
  },
  texto:{ 
    fontWeight: 'bold', 
    fontSize: 18 }
});

export default CropIrrigation;