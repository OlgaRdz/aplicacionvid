import React, { useState, useEffect } from "react";
import { StyleSheet, SafeAreaView, FlatList, ScrollView } from "react-native";
import firebase from '../Components/Firebase';
import { Avatar, Card, Title, Paragraph } from 'react-native-paper';
import "firebase/database"
import 'firebase/storage';

const Bugs = ({ route }) => {
    //console.log(route.params);
    const LeftContent = props => <Avatar.Icon {...props} icon="bug" />


    const [bugsImages, guardarbugs] = useState([]);
    const [url, setUrl] = useState();
    const idFurrow = route.params.idfurrow
    const nameFurrow = route.params.namefurrow

    //const { Name, Type, humidity } = route.params.Valors.plot4 || route.params.Valors.plot3 || route.params.Valors.plot2 || route.params.Valors.plot1
    var photoid = []


    // function photofun() {
    //         var vabugs = firebase.database().ref('Photo');
    //         vabugs.orderByChild('id_furrow').equalTo(photof).on('value', (snapshot) => {
    //             guardarbugs(snapshot.val())
    //             console.log(bugs)

    //             snapshot.forEach((photosnapshot) => {
    //                 const { data, hour, id_furrow, photo } = photosnapshot.val()
    //                 photoid = photo
    //                 console.log(photoid)

    //                 let imageRef = firebase.storage().ref(photoid);
    //                 imageRef
    //                     .getDownloadURL()
    //                     .then((url) => {

    //                         setUrl(url);
    //                     })
    //                     .catch((e) => console.log(e))
    //                     console.log(url) 
    //             });
    //         });
    //      return url; 


    // }
    // photofun();

    function obtenerbugs() {
        var vabugs = firebase.database().ref('Photo');
        vabugs.orderByChild('id_furrow').equalTo(idFurrow).on('value', (snapshot) => {
            var photos = snapshot.val();
            guardarbugs(photos);
            console.log("Primeras fotos")
            console.log(photos);


        });
    }

    useEffect(() => {
        const obtenerFurrows = () => {
            try {
                obtenerbugs();
            } catch (error) {
                console.log(error);
            }
        }
        obtenerFurrows();




    }, []);

    const newPhotos = Object.keys(bugsImages).map((key) => {
        let url = firebase.storage().ref('/' + bugsImages[key].photo).getDownloadURL();
        const nameFurrow = firebase.database().ref('Furrows').equalTo(bugsImages[key].id_furrow).on('value', (snapshot) => {
            var photos = snapshot.val();
            guardarbugs(photos);
            console.log("Primeras fotos")
            console.log(photos);


        });
        bugsImages[key].url = url;
        return bugsImages[key];
      });

    const Item = ({ item, onPress, style, }) => (

        <ScrollView style={{ paddingHorizontal: 20, backgroundColor: "#fff", flex: 1 }}>
            <Card>
                <Card.Title title={name} subtitle="Summary: A total of 50 infected vines were found" left={LeftContent} />
                <Card.Cover source={{ uri: item.url }} />


                <Card.Content style={styles.container}>
                    <Title>{item.photoid}</Title>
                    <Paragraph>{item.url}10 matches found</Paragraph>


                </Card.Content>


            </Card>


        </ScrollView>

    );



    const [selectedId, setSelectedId] = useState(null);

    const renderItem = ({ item }) => {
        const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';


        return <Item item={item} onPress={() => setSelectedId(item.id)} style={{ backgroundColor }} />;

    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <FlatList
                data={newPhotos}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                extraData={selectedId}
            />
        </SafeAreaView>
    );
}






const styles = StyleSheet.create({
    container: {
        backgroundColor: "#cfd8dc"


    },
    container1: {
        width: '100%',
        height: 140,

        alignItems: "center",
        justifyContent: "center",

        marginVertical: 5,
        borderRadius: 60,
        borderColor: '#b3bcc2',
        borderWidth: 3,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,

    },


});

export default Bugs;