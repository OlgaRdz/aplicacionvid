import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { API_KEY } from '../utilities/WeatherAPIKey';
import Weather from '../Components/weather';

export default class App extends React.Component {
  state = {
    isLoading: true,
    temperature: 0,
    weatherCondition: null,
    city: "Brooklyn",
    country: "US",
    error: null,
    humidity :0,
    wind : 0,
    visibility:0,
    precipitation: 0,
    uv:8,
    calor: 0,
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.fetchWeather(position.coords.latitude, position.coords.longitude);
      },
      error => {
        this.setState({
          error: 'Error Getting Weather Condtions'
        });
      }
    );
  }

  fetchWeather(lat, lon) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then(res => res.json())
      .then(json => {
        this.setState({
          temperature: Math.round(json.main.temp),
          humidity: Math.round(json.main.humidity ),
          wind: Math.round((json.wind.speed * 18) / 5),
          
          
          
          calor : Math.round(json.main.temp),
         
          weatherCondition: json.weather[0].main,
          city: json.name,
          country: json.sys.country,
          isLoading: false
        });
      });
  }
  fetchWeather1(lat, lon) {
    fetch(
      `http://history.openweathermap.org/data/2.5/history/accumulated_precipitation?lat=${lat}&lon=${lon}&start=1586853378&end=1589445367&appid=${API_KEY}`
    )
      .then(res => res.json())
      .then(json => {
        // console.log(json);
        this.setState({
          
          precipitation : this.fetchWeather1,
          isLoading: false
        });
      });
  }
  fetchWeather2(lat, lon) {
    fetch(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${API_KEY}
`
    )
      .then(res => res.json())
      .then(json => {
        // console.log(json);
        this.setState({
         
          visibility : 'hola',
          
          isLoading: false
        });
      });
  }
  

  render() {
    const { isLoading, weatherCondition, temperature, city, country, humidity,wind,visibility,precipitation,calor } = this.state;
    return (
      <View style={styles.container}>
        {isLoading ? (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>Fetching The Weather</Text>
          </View>
        ) : (
          <Weather weather={weatherCondition} temperature={temperature} city={city} country={country} humidity={humidity}  wind={wind} visibility={visibility} precipitation={precipitation} calor={calor} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFDE4'
  },
  loadingText: {
    fontSize: 30
  }
});