import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, Text } from 'react-native';
import { StackedBarChart } from 'react-native-svg-charts';

const data = [
    {
        month: new Date(2015, 0, 1),
        apples: 1000,
        bananas: 0,
        cherries: 0
    },
    {
        month: new Date(2015, 1, 1),
        apples: 0,
        bananas: 800,
        cherries: 0
    },
    {
        month: new Date(2015, 2, 1),
        apples: 0,
        bananas: 0,
        cherries: 500
    },
]

const colors = ['#870606', '#9b2d2d', '#bd7a7a']
const keys = ['apples', 'bananas', 'cherries']

class Statistics extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View >
                        <Text style={styles.textDate}>This Week                                   <Text style={styles.textTime}>10 h 18 min</Text> </Text>
                        <StackedBarChart
                            style={styles.chartStyle}
                            keys={keys}
                            colors={colors}
                            data={data}
                            horizontal={true}
                            showGrid={false}
                            contentInset={{ top: 20, bottom: 60 }}
                        />
                    </View>
                    <View >
                    <Text style={styles.textDate}>This Month                                 <Text style={styles.textTime}>10 h 18 min</Text> </Text>
                        <StackedBarChart
                            style={styles.chartStyle}
                            keys={keys}
                            colors={colors}
                            data={data}
                            horizontal={true}
                            showGrid={false}
                            contentInset={{ top: 20, bottom: 60 }}
                        />
                    </View>
                    <View>
                    <Text style={styles.textDate}>This Year                                   <Text style={styles.textTime}>10 h 18 min</Text> </Text>
                        <StackedBarChart
                            style={styles.chartStyle}
                            keys={keys}
                            colors={colors}
                            data={data}
                            horizontal={true}
                            showGrid={false}
                            contentInset={{ top: 20, bottom: 60 }}
                        />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    textDate: { 
        fontSize: 20 
    },
    textTime: {
        fontSize: 15, 
        color: '#b6beca'
    },
    chartStyle: { 
        height: 200 
    }
});
export default Statistics;