import React, { useState, useEffect } from "react";
import { View, Switch, StyleSheet, Text, TouchableOpacity } from "react-native";
import firebase from '../Components/Firebase';
import "firebase/database"
import "firebase/auth";

const CropHumidity = (props) => {

  // VARIABLES FECHA
  const [currentDate, setCurrentDate] = useState('');
  const [currentDate1, setCurrentDate1] = useState('');

  //VARIABLES ARREGLO PLOT
  const [plot1, guardarPlot1] = useState([]);
  const [plot2, guardarPlot2] = useState([]);
  const [plot3, guardarPlot3] = useState([]);
  const [plot4, guardarPlot4] = useState([]);
  var plot = null;

  // START FECHA
  useEffect(() => {

    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    setCurrentDate(
      date + '/' + month
    );
    setCurrentDate1(
      hours + ':' + min + ':' + sec

    );
  }, []);

  // END FECHA

  //START CONSULTA DE PLOTS
  function consultarPlot1() {
    var vaplot = firebase.database().ref('Plots/p_001');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      guardarPlot1(plot);

    });

  }
  function consultarPlot2() {
    var vaplot = firebase.database().ref('Plots/p_002');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      guardarPlot2(plot);

    });

  }
  function consultarPlot3() {
    var vaplot = firebase.database().ref('Plots/p_003');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      guardarPlot3(plot);

    });

  }
  function consultarPlot4() {
    var vaplot = firebase.database().ref('Plots/p_004');
    vaplot.on('value', (snapshot) => {
      plot = snapshot.val();
      guardarPlot4(plot);
    });
  }

  useEffect(() => {
    const consultarPlots = async () => {
      try {
        consultarPlot1();
        consultarPlot2();
        consultarPlot3();
        consultarPlot4();

      } catch (error) {
        console.error(error);
      }
    }
    consultarPlots();

  }, []);
  //END CONSULTA PLOTS

  const cambiosValvula1 = () => {
    firebase.database().ref('Plots/p_001').update({
      stateSensor: !plot1.stateSensor,
    });
  }
  const cambiosValvula2 = () => {
    firebase.database().ref('Plots/p_002').update({
      stateSensor: !plot2.stateSensor,
    });
  }
  const cambiosValvula3 = () => {
    firebase.database().ref('Plots/p_003').update({
      stateSensor: !plot3.stateSensor,
    });
  }
  const cambiosValvula4 = () => {
    firebase.database().ref('Plots/p_004').update({
      stateSensor: !plot4.stateSensor,
    });
  }

  // END CONTROL DE VALVULAS

  return (


    <View style={styles.container}>
      <View style={styles.container1}>

        <Text style={styles.Date} >{currentDate}</Text>
        <Text style={styles.Date1}>{currentDate1}</Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate("CropIrrigation")}

          style={styles.Button1}>
          <Text style={styles.textbutton} > PUMP </Text>
        </TouchableOpacity>


      </View>

      <View style={styles.countContainer}>
        <Text style={styles.textTitle}></Text>
      </View>

      <View style={styles.listado}>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot4.humidity ? styles.buttonGreen : styles.button} >
            <Text style={styles.textBody}>{plot4.Name}</Text>
            <Text style={styles.textBodyName}>{plot4.Type}</Text>
            <Text style={styles.textBody}>{plot4.humidity ? "Perfect Condition" : "Dry Furrows"}</Text>
            { }
            {plot4.humidity ? null : (
              <Switch
                style={{ left: 1, top: 7 }}
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor="#f5dd4b"
                ios_backgroundColor="#3e3e3e"
                onValueChange={cambiosValvula4}
                value={plot4.stateSensor}

              />
            )}
          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot2.humidity ? styles.buttonGreen : styles.button} >
            <Text style={styles.textBody}>{plot2.Name}</Text>
            <Text style={styles.textBodyName}>{plot2.Type}</Text>
            <Text style={styles.textBody}>{plot2.humidity ? "Perfect Condition" : "Dry Furrows"}</Text>
            {plot2.humidity ? null : (
              <Switch
                style={{ left: 1, }}
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor="#f5dd4b"
                ios_backgroundColor="#3e3e3e"
                onValueChange={cambiosValvula2}
                value={plot2.stateSensor}

              />
            )}

          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot3.humidity ? styles.buttonGreen : styles.button} >
            <Text style={styles.textBody}>{plot3.Name}</Text>
            <Text style={styles.textBodyName}>{plot3.Type}</Text>
            <Text style={styles.textBody}>{plot3.humidity ? "Perfect Condition" : "Dry Furrows"}</Text>
            {plot3.humidity ? null : (
              <Switch
                style={{ left: 1, top: 7 }}
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor="#f5dd4b"
                ios_backgroundColor="#3e3e3e"
                onValueChange={cambiosValvula3}
                value={plot3.stateSensor}

              />
            )}
          </TouchableOpacity>
        </View>

        <View style={styles.listadoItem}>
          <TouchableOpacity style={plot1.humidity ? styles.buttonGreen : styles.button} >
            <Text style={styles.textBody}>{plot1.Name}</Text>
            <Text style={styles.textBodyName}>{plot1.Type}</Text>
            <Text style={styles.textBody}>{plot1.humidity ? "Perfect Condition" : "Dry Furrows"}</Text>
            {plot1.humidity ? null : (
              <Switch
                style={{ left: 1, }}
                trackColor={{ false: "#767577", true: "#81b0ff" }}
                thumbColor="#f5dd4b"
                ios_backgroundColor="#3e3e3e"
                onValueChange={cambiosValvula1}
                value={plot3.stateSensor}

              />
            )}
          </TouchableOpacity>
        </View>

      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 40,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#dbcbbe",
    borderRadius: 15,
    width: '100%',
    height: 150,
    marginVertical: 5,
    marginHorizontal: 2,
    padding: 10,
    borderColor: 'black',
    borderWidth: 2,
  },
  countContainer: {
    alignItems: "center",
    padding: 10
  },
  buttonGreen: {
    alignItems: "center",
    backgroundColor: "#a8ea7f",
    borderRadius: 15,
    width: '100%',
    height: 150,
    marginVertical: 5,
    marginHorizontal: 2,
    padding: 10,
    borderColor: 'black',
    borderWidth: 2,
  },
  textTitle: {
    fontSize: 30,
    color: 'black',
    fontWeight: 'bold'
  },
  textBody: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    fontWeight: 'bold'
  },
  textBodyName: {
    textAlign: 'center',
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold'
  },
  listado: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  listadoItem: {
    flexBasis: '48%'
  },
  container1: {
    flex: 50,
    alignItems: "center",
    justifyContent: "center",
    padding: 40,
    top: 5,

  },
  Button1: {
    width: 100,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#bd0045',
  },
  textbutton: {
    textAlign: 'center',
    fontSize: 11,
    marginTop: 0,
    width: 200,
    color: "#fff"

  },
  textbutton1: {
    textAlign: 'center',
    fontSize: 20,
    marginTop: 0,
    width: 150,
    color: "black"
  },
  Date: {

    fontSize: 30,
    fontWeight: "bold"
  },
  Date1: {

    fontSize: 20,

  },
});

export default CropHumidity;