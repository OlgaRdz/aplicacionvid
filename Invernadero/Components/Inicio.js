import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../LoginScreen';
import ForgotPass from '../screens/ForgotPassword'

const Inicio = () => {
  const Stack = createStackNavigator();

  return (  
        <Stack.Navigator screenOptions={{
          headerShown: false
        }}>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
          />
          <Stack.Screen
            name="ForgotPass"
            component={ForgotPass}
          />
        </Stack.Navigator>
  );
}
 
export default Inicio;