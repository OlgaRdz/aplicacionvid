import firebase from "firebase/app";

var firebaseConfig = {
  apiKey: "AIzaSyBL9qQgrHzYUan_ErVEYCEkQwLByF0yguQ",
  authDomain: "vineyard-6c024.firebaseapp.com",
  databaseURL: "https://vineyard-6c024-default-rtdb.firebaseio.com",
  projectId: "vineyard-6c024",
  storageBucket: "vineyard-6c024.appspot.com",
  messagingSenderId: "85440077086",
  appId: "1:85440077086:web:2480a7d60a7b6649ccfd0f"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}else {
  firebase.app(); // if already initialized, use that one
}

export default firebase;
