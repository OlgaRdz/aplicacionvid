import 'react-native-gesture-handler';
import React, { useState } from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-native-paper';
import Statistics from '../screens/Statistics';
import Irrigation from '../screens/Irrigation';
import CropHumidity from "../screens/CropHumidity"
import CropIrrigation from "../screens/CropIrrigation"
import WeatherCondition from "../screens/WeatherCondition"
import Plagues from "../screens/Plagues"
import CreateUser from '../screens/CreateUser'
import OptionsMenu from './OptionsMenu';
import Furrows from '../screens/Furrows';
import Bugs from '../screens/Bugs'
import "firebase/auth";

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

function TabStack() {


  return (

    <Tab.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        activeTintColor: '#FFFFFF',
        inactiveTintColor: '#ffffff',
        style: {
          backgroundColor: '#bd0045',
        },
        labelStyle: {
          textAlign: 'center',
        },
        indicatorStyle: {
          borderBottomColor: '#ffffff',
          borderBottomWidth: 2,
        },
        showIcon: true,
        showLabel: false,
      }}>

      <Tab.Screen
        name="Statistics"
        component={Statistics}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Ionicons name="stats-chart" size={24} color="white" />
            ) : (
              <Ionicons name="stats-chart" size={24} color="#870606" />
            ),

        }} />
      <Tab.Screen
        name="Irrigation"
        component={Irrigation}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Ionicons name="water" size={24} color="white" />
            ) : (
              <Ionicons name="water" size={24} color="#870606" />
            ),
        }} />
      <Tab.Screen
        backgroundColor="black"
        name="ThirdPage"
        component={Plagues}
        options={{
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Ionicons name="bug" size={24} color="white" />
            ) : (
              <Ionicons name="bug" size={24} color="#870606" />
            ),
        }} />
    </Tab.Navigator>

  );
}

const Home = ({ route }) => {
  return (
    <Provider>

      <Stack.Navigator
        initialRouteName="Settings"
        screenOptions={({ navigation }) => ({
          headerStyle: { backgroundColor: '#bd0045' },
          headerTintColor: '#fff',
          headerTitleStyle: { fontWeight: 'bold' },
          headerTitleStyle: { alignSelf: 'center' },

          headerRight: (props) => <OptionsMenu {...props}
            navigation={navigation} />

        })}>

        <Stack.Screen
          name="TabStack"
          component={TabStack}
          options={{
            headerLeft: () => null,
            headerTitle: () => (
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.headerText}>Welcome to Vineyard Cares</Text>
              </View>
            ),
          }}
        />

        <Stack.Screen
          name="CropHumidity"
          component={CropHumidity}
          options={{
            title: "CropHumidity",
            headerTitleStyle: { alignSelf: 'center' },
            headerTitle: () => (
              <View style={styles.viewIcon}>
                <MaterialCommunityIcons name="water-percent" size={28} color="white" />
                <Text style={styles.headerText}>Crop Humidity</Text>
              </View>
            ),
          }}
        />

        <Stack.Screen
          name="CropIrrigation"
          component={CropIrrigation}
          options={{
            title: "CropIrrigation",
            headerTitleStyle: { alignSelf: 'center' },
            headerTitle: () => (
              <View style={styles.viewIcon}>
                <MaterialCommunityIcons name="water-pump" size={28} color="white" />
                <Text style={styles.headerText}>Crop Irrigation</Text>
              </View>
            ),
          }}
        />

        <Stack.Screen
          name="WeatherCondition"
          component={WeatherCondition}
          options={{
            title: "Weather Condition",
            headerTitleStyle: { alignSelf: 'center' },
            headerTitle: () => (
              <View style={styles.viewIcon}>
                <Ionicons name="thunderstorm-outline" size={28} color="white" />
                <Text style={styles.headerText}>Weather Condition</Text>
              </View>
            ),
          }}
        />

        <Stack.Screen
          name="CreateUser"
          component={CreateUser}
          options={{
            headerTitle: () => (
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.headerText}>Create User</Text>
              </View>
            ),
          }}
        />
        <Stack.Screen
          name="Furrows"
          component={Furrows}
          options={({ route }) => ({
            title: route.params.name,
            headerTitleStyle: {
              fontWeight: 'bold',
              fontSize: 20
            },
          })}

        />
        <Stack.Screen
          name="Furrows-Bugs"
          component={Bugs}
          options={{

            headerTitle: () => (
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.headerText}>Furrows-Bugs</Text>
              </View>
            ),
          }}
        />

      </Stack.Navigator>
    </Provider>
  );
}
const styles = StyleSheet.create({
  headerText: {
    color: '#fff',
    alignSelf: 'flex-start', 
    fontSize: 20, 
    fontWeight: "bold", 
    paddingLeft: 10
  },
  viewIcon: { 
    flexDirection: 'row', 
    marginLeft: -20 
  }
});

export default Home;