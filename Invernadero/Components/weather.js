import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { weatherConditions } from '../utilities/weatherConditions';


const Weather = ({ weather, temperature, humidity, wind,precipitation,}) => {
  const [currentDate, setCurrentDate] = useState('');
  const [currentTime, setCurrentTime] = useState('');

  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    if (month == 1) {
      month = 'January'
    }
    if (month == 2) {
      month = 'February'
    }
    if (month == 3) {
      month = 'March'
    }
    if (month == 4) {
      month = 'April'
    }
    if (month == 5) {
      month = 'May'
    }
    if (month == 6) {
      month = 'June'
    }
    if (month == 7) {
      month = 'July'
    }
    if (month == 8) {
      month = 'August'
    }
    if (month == 9) {
      month = 'September'
    }
    if (month == 10) {
      month = 'October'
    }
    if (month == 11) {
      month = 'November'
    }
    if (month == 12) {
      month = 'December'
    }
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    setCurrentDate(
      month + ' ' + date + ', ' + year
    );
    setCurrentTime(
      hours + ':' + min
    )
  }, []);
  return (
    <View style={[styles.weatherContainer, { backgroundColor: '#fff' }]} >

      <View style={styles.headerContainer }>
        <Text style={styles.tempText}>{temperature}˚C</Text>
        <MaterialCommunityIcons style={{ alignItems:'baseline' }} size={100} name={weatherConditions[weather].icon} color={'black'} />
      </View>

      <View style={styles.subHeaderContainer}>
        <Text style={styles.subText}>{currentDate}</Text>
        <Text style={styles.subText}>{currentTime}</Text>
      </View>

      <View style={styles.bodyContainer}>
        <TouchableOpacity style={styles.button} >
          <Image source={require('../assets/images/humidity.png')} style={styles.imagen} />
          <Text style={styles.textTitle}>{humidity}%</Text>
          <Text style={styles.textSubText}>Humidity</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} >
          <Image source={require('../assets/images/viento.png')} style={styles.imagen} />
          <Text style={styles.textTitle}>{wind} km/h</Text>
          <Text style={styles.textSubText}>Wind</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} >
          <Image source={require('../assets/images/rainfall.png')} style={styles.imagen} />
          <Text style={styles.textTitle}>{precipitation}%</Text>
          <Text style={styles.textSubText}>Precipitation</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} >
          <Image source={require('../assets/images/brightness.png')} style={styles.imagen} />
          <Text style={styles.textTitle}> UV</Text>
          <Text style={styles.textSubText}>Light Intensity</Text>
        </TouchableOpacity>
      </View>

    </View>
  );
};

Weather.propTypes = {
  temperature: PropTypes.number.isRequired,
  weather: PropTypes.string
};

const styles = StyleSheet.create({
  weatherContainer: {
    flex: 1
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 0
  },
  subHeaderContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 30,
  },
  tempText: {
    fontSize: 80,
    color: 'black',
    paddingLeft: 10,
  },
  subText: {
    fontSize: 28,
    color: 'black'
  },
  bodyContainer: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingLeft: 30,
    flexWrap: 'wrap',
  },
  button: {
    alignItems: "center",
    backgroundColor: "#fff",
    borderRadius: 15,
    width: 170,
    height: 100,
    marginVertical: 5,
    marginHorizontal: 2,
    padding: 10,
    borderWidth: 3,
    borderColor: '#d1d6dd'
  },
  textTitle: {
    fontSize: 18,
    color: '#5a636e',
    fontWeight: 'bold',
    paddingLeft: 60,
    top: 12
  },
  textSubText: {
    fontSize: 12,
    color: '#5a636e',
    fontWeight: 'bold',
    paddingLeft: 60,
    top: 12
  },
  imagen: { 
    width: 50, 
    height: 50, 
    position: 'absolute', 
    left: 15, 
    top: 25 }
});

export default Weather;
