import React, { useState, useEffect } from "react";
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { Menu } from 'react-native-paper';
import firebase from './Firebase';
import "firebase/auth";
import { Ionicons } from '@expo/vector-icons';
import "firebase/database"

const OptionsMenu = ({ navigation }) => {
  const [menuVisible, setMenuVisible] = useState(false);
  const openMenu = () => setMenuVisible(true);
  const closeMenu = () => setMenuVisible(false);
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    const permiso = () => {
      var user = firebase.auth().currentUser;
      var iduser = user.uid;
      var db = firebase.database().ref(`Admins/UID/${iduser}`);

      db.on('value', (snapshot) => {
        var data = snapshot.val();
        setIsAdmin(data)
      });
    }
    permiso();
  }, []);


  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
      }}>
      <Menu
        visible={menuVisible}
        onDismiss={closeMenu}
        anchor={<TouchableOpacity onPress={openMenu} style={{ paddingRight: 10 }}><Ionicons name="ellipsis-vertical" size={24} color="white" /></TouchableOpacity >}>
        {isAdmin == true ? <Menu.Item onPress={() => navigation.navigate('CreateUser')} title="Create account" /> : null}
        <Menu.Item onPress={() => firebase.auth().signOut()} title="Sign Out" />
      </Menu>
    </View>
  );
}

export default OptionsMenu;