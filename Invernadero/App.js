import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Home from './Components/Home';
import Inicio from './Components/Inicio';
import firebase from './Components/Firebase';
import "firebase/auth";

class App extends React.Component {
  state = {
    loggedIn: false
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        if (user.emailVerified) {
          this.setState({
            loggedIn: true,

          });
        } else if (user.email === "admin@gmail.com") {
          this.setState({
            loggedIn: true,

          });
        }
      } else {
        this.setState({
          loggedIn: false
        })
      }
    })
  }

  renderContent = () => {
    switch (this.state.loggedIn) {
      case false:
        return <Inicio />

      case true:
        return <Home />
    }
  }

  render() {
    return (
      <NavigationContainer>
        {this.renderContent()}
      </NavigationContainer>
    );
  }
}

export default App;